﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Script : MonoBehaviour {

    [SerializeField] GameObject explosionPrefab;
    [SerializeField] Transform  explosionParent;

    GameObject explosionObject;
    ScoreBoard scoreBoard;
    Vector3 initialPosition;
    Vector3 motionDirection;
    float pulseSpeed = 2.0f;

    // Use this for initialization
    void Start () {
        AddNonTriggerBoxCollider();
        scoreBoard = FindObjectOfType<ScoreBoard>();
        initialPosition = transform.localPosition;
        motionDirection = new Vector3(10f, 10f, 0);
    }
	
    void AddNonTriggerBoxCollider()
    {
        Collider boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = false;
    }

    private void Update()
    {
        float newPos = Mathf.Cos(Time.time * pulseSpeed);
        transform.localPosition = initialPosition + motionDirection * newPos; 

        float newRotate = Time.deltaTime;

        transform.Rotate(newRotate * 90f, 0, 0);
    }

    void OnParticleCollision(GameObject particleObject)
    {
        explosionObject = Instantiate(explosionPrefab, explosionParent);
        explosionObject.transform.SetPositionAndRotation(transform.position, transform.rotation);
        explosionObject.SetActive(true);
        gameObject.SetActive(false);
        scoreBoard.AddToScore(100);
        Invoke("DestroyObjects", 5f);
    }

    void DestroyObjects()
    {
        Destroy(explosionObject);
        Destroy(gameObject);
    }
}
