﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1Script : MonoBehaviour {

    [SerializeField] GameObject explosionPrefab;
    [SerializeField] Transform  explosionParent;

    GameObject explosionObject;
    ScoreBoard scoreBoard;
    Vector3 initialScale;
    float pulseSpeed = 2.0f;

    // Use this for initialization
    void Start () {
        AddNonTriggerBoxCollider();
        scoreBoard = FindObjectOfType<ScoreBoard>();
        initialScale = transform.localScale;
	}
	
    void AddNonTriggerBoxCollider()
    {
        Collider boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = false;
    }

    private void Update()
    {
        float newScale = 0.66f + 0.33f * Mathf.Cos(Time.time * pulseSpeed);
        transform.localScale = initialScale * newScale;

        float newRotate = Time.deltaTime;

        transform.Rotate(newRotate * 10f, newRotate * 20f, newRotate * 30f);
    }

    void OnParticleCollision(GameObject particleObject)
    {
        explosionObject = Instantiate(explosionPrefab, explosionParent);
        explosionObject.transform.SetPositionAndRotation(transform.position, transform.rotation);
        explosionObject.SetActive(true);
        gameObject.SetActive(false);
        scoreBoard.AddToScore(100);
        Invoke("DestroyObjects", 5f);
    }

    void DestroyObjects()
    {
        Destroy(explosionObject);
        Destroy(gameObject);
    }
}
