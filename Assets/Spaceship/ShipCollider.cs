﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCollider : MonoBehaviour {

    [SerializeField] ViewMovers gameplay;
   
    void OnTriggerEnter(Collider other)
    {
//        gameplay.SendMessage("OnCollideShip", this, other);

        // Forward to gameplay object
        gameplay.OnCollideShip(this, other);
    }
}
