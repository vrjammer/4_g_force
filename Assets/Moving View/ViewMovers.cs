﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ViewMovers : MonoBehaviour {

    [Header("Camera")]
    [SerializeField] Transform cameraContainer;
    [SerializeField] Camera    followingCamera;

    [Header("Spaceship")]
    [SerializeField] Transform trails;
    [SerializeField] Transform projectiles;
    [SerializeField] Rigidbody spaceship;
    [SerializeField] Transform spaceshipHull;
    [SerializeField] Vector2   displacementLimitsMax;
    [SerializeField] Vector2   displacementLimitsMin;
    [SerializeField] GameObject[] guns;

    [Header("Physical Motion")]
    [SerializeField] float ropingForce;
    [SerializeField] float dampeningForce;
    [SerializeField] Transform trackedObject;
    [SerializeField] Transform waypointTarget;
    [SerializeField] Transform initialWaypoint;

    [Header("Explosion")]
    [SerializeField] ParticleSystem explosion;

    [Header("Gameplay")]
    [Tooltip("In seconds")][SerializeField]float deathReloadingDelay;

    Vector3 deltaCameraPosition;
    Vector3 deltaShipPosition;
    Vector3 cameraDefaultOffset;
    Vector3 targetRotateShip;
    Vector3 currentRotateShip;
    const float rotationSpeed = 20f;

    bool firing = false;

    enum GameState { normal, isDead};

    GameState currentGameState = GameState.normal;

    // Use this for initialization
    void Start () {
        deltaShipPosition = new Vector3(0, 0, 0);
        deltaCameraPosition = new Vector3(0, 0, 0);
        cameraDefaultOffset = followingCamera.transform.localPosition;
        trackedObject.SetPositionAndRotation(initialWaypoint.position, initialWaypoint.rotation);
        foreach (GameObject gun in guns)
        {
            gun.SetActive(false);
        }
        currentRotateShip = new Vector3(0, 0, 0);
        targetRotateShip = new Vector3(0, 0, 0);
    }

    void ApplyShipRotation(Vector3 horizontalVelocity)
    {
        spaceship.transform.Rotate(currentRotateShip);
        spaceshipHull.transform.SetPositionAndRotation(spaceship.transform.position, spaceship.transform.rotation);
        trails.transform.SetPositionAndRotation(spaceship.transform.position, spaceship.transform.rotation);
        projectiles.transform.SetPositionAndRotation(spaceship.transform.position, spaceship.transform.rotation);
        for (int i = 0; i < projectiles.childCount; ++i)
        {
            Transform child = projectiles.GetChild(i);
            child.LookAt(child.position - horizontalVelocity);
        }

        cameraContainer.transform.SetPositionAndRotation(trackedObject.localPosition, trackedObject.localRotation);
        cameraContainer.transform.LookAt(cameraContainer.transform.position - horizontalVelocity);

        Vector3 newCameraPosition = new Vector3((deltaCameraPosition.x + cameraContainer.transform.InverseTransformPoint(spaceship.transform.position).x * 3f) * 0.25f,
                                                (deltaCameraPosition.y + cameraContainer.transform.InverseTransformPoint(spaceship.transform.position).y * 3f) * 0.25f,
                                                 deltaCameraPosition.z + cameraContainer.transform.InverseTransformPoint(spaceship.transform.position).z);

        followingCamera.transform.localPosition = cameraDefaultOffset + newCameraPosition;
    }

    void HandleInput()
    {
        float deltaTime = Time.fixedDeltaTime;

        targetRotateShip = new Vector3(0, 0, 0);
        if (Input.GetKey(KeyCode.A))
        {
            if (deltaShipPosition.x < displacementLimitsMax.x)
            {
                deltaShipPosition.x += 20.0f * deltaTime;
            }
            targetRotateShip.z = -20f;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (deltaShipPosition.x > displacementLimitsMin.x)
            {
                deltaShipPosition.x -= 20.0f * deltaTime;
            }
            targetRotateShip.z = 20f;
        }
        if (Input.GetKey(KeyCode.W))
        {
            if (deltaShipPosition.y < displacementLimitsMax.y)
            {
                deltaShipPosition.y += 20.0f * deltaTime;
            }
            targetRotateShip.x = 10f;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            if (deltaShipPosition.y > displacementLimitsMin.y)
            {
                deltaShipPosition.y -= 20.0f * deltaTime;
            }
            targetRotateShip.x = -10f;
        }
        if (Input.GetKey(KeyCode.Space) && !firing)
        {
            firing = true;
            foreach(GameObject gun in guns)
            {
                gun.SetActive(true);
            }
        }
        else if (!Input.GetKey(KeyCode.Space) && firing)
        {
            firing = false;
            foreach (GameObject gun in guns)
            {
                gun.SetActive(false);
            }
        }

    }

    // Update is called once per frame
    void Update () {
        Vector3 velocity = waypointTarget.position - trackedObject.position;
        velocity = velocity * 20.0f / velocity.magnitude;
        Vector3 horizontalVelocity = new Vector3(velocity.x, 0, velocity.z);

        spaceship.transform.LookAt(spaceship.position - horizontalVelocity);

        ApplyShipRotation(horizontalVelocity);
    }

    void FixedUpdate()
    {
        float deltaTime = Time.fixedDeltaTime;

        Vector3 velocity = waypointTarget.position - trackedObject.position;
        velocity = velocity.magnitude != 0 ? velocity * 20.0f / velocity.magnitude : velocity;

        Vector3 horizontalVelocity = new Vector3(velocity.x, 0, velocity.z);

        if (currentGameState == GameState.normal)
        {
            HandleInput();

            trackedObject.Translate(velocity * deltaTime);
        }

        spaceship.transform.LookAt(spaceship.position - horizontalVelocity);

        Vector3 distanceVector = spaceship.transform.position - trackedObject.position - spaceship.transform.TransformVector(deltaShipPosition);

        spaceship.AddForce(-distanceVector * ropingForce);
        spaceship.AddForce((horizontalVelocity - spaceship.velocity) * dampeningForce);

        if (rotationSpeed * deltaTime < 1f)
        {
            currentRotateShip = (1f - rotationSpeed * deltaTime) * currentRotateShip + rotationSpeed * deltaTime * targetRotateShip;
        }
        else
        {
            currentRotateShip = targetRotateShip * 1f;
        }
        ApplyShipRotation(horizontalVelocity);
    }

    public void OnCollideShip(ShipCollider ship, Collider other)
    {
        if (other.CompareTag("Enemy") ||
            other.CompareTag("Obstacle"))
        {
            explosion.transform.SetPositionAndRotation(spaceship.transform.position, spaceship.transform.rotation);
            explosion.gameObject.SetActive(true);
            trails.gameObject.SetActive(false);
            projectiles.gameObject.SetActive(false);
            spaceshipHull.gameObject.SetActive(false);
            currentGameState = GameState.isDead;
            Invoke("DeathReloading", deathReloadingDelay);
        }
        else
        {
            print("Unknown collider tag : " + other.tag);
        }
    }

    void DeathReloading()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}

