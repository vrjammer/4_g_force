﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroScript : MonoBehaviour {

    private AudioSource thisSource;

    void Awake()
    {
        Object.DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start () {
        SceneManager.sceneLoaded += OnSceneLoaded;
        thisSource = GetComponent<AudioSource>();
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        float startTime = Time.time;
        float startVolume = thisSource.volume;

        while(Time.time <= startTime + 3f)
        {
            thisSource.volume = startVolume * (3f - Time.time + startTime) / 3f;
            yield return null;
        }

        Object.Destroy(this.gameObject);
    }

}
